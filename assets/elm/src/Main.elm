module Main exposing (..)

import Html exposing (Html, Attribute, div, input, text, button, h2)
import Html.Attributes exposing (.. )
import Html.Attributes.Aria exposing (ariaLabel)
import Html.Events exposing (onInput)

main =
  Html.beginnerProgram { model = model, view = view, update = update }

-- MODEL

type alias Model =
  { query : String }

model : Model
model =
  Model ""

-- UPDATE
type Msg = 
  Query String

update: Msg -> Model -> Model

update msg model =
  case msg of
    Query query -> 
      { model | query = query }

-- VIEW

view: Model -> Html Msg

view model = 
 div [containerStyle] 
 [
  h2 [class "title"] [text "Yroo"],
  Html.form [method "get", action "/search", formStyle]
  [ input [ class "search-bar", type_ "text", placeholder "Search millions of products...", onInput Query, name "search[query]", inputStyle, ariaLabel "search"] []
  , button [buttonStyle] [text "Search"]
  ]
 ] 
  
containerStyle =
  style
    [("text-align", "center")
    , ("margin-top", "120px")
    ]
formStyle =
  style
    [("text-align", "center")
    , ("margin-top", "40px") 
    ]   
inputStyle =
  style
    [ ("width", "35%")
    , ("height", "30px")
    , ("padding", "10px 0 10px 20px")
    , ("font-size", "16px")
    ]
buttonStyle =
  style
    [ ("width", "5%")
    , ("padding", "10px 0")
    , ("font-size", "16px")
    , ("text-align", "center")
    , ("line-height", "30px")
    , ("cursor", "pointer")
    ]