module MainTests exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Html
import Html.Attributes exposing (..)
import Test.Html.Query as Query
import Test.Html.Selector exposing (text, tag)
import Main

suite : Test
suite =
  describe "Main"
    [ describe "Model"
        [ test "sets query to given argument" <|
            \_ ->
              Main.Model "vacuum"
                |> Expect.equal {query = "vacuum"}
        ]
      ,
      describe "Update"
        [ test "updates model with new query" <|
            \_ ->
              Main.Model ""
                |> Main.update(Main.Query "shark")
                |> Expect.equal { query = "shark" }
        ]
      ,

      describe "View"
        [
          test "Page title has the text yroo" <|
            \_ ->
              Main.view(Main.Model("vacuum"))
                |> Query.fromHtml
                |> Query.find [ tag "h2" ]
                |> Query.has [ text "Yroo" ]
          ,
          
          test "Search button has the text search" <|
            \_ ->
              Main.view(Main.Model("vacuum"))
                |> Query.fromHtml
                |> Query.find [ tag "button" ]
                |> Query.has [ text "Search" ]
                      
        ]  
    ]