self.addEventListener('install', e => {
 e.waitUntil(
   caches.open('cache-v1').then(cache => {
     return cache.addAll(['/']);
   })
 );
});