// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"
import Elm from "./main.js";
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

if ('serviceWorker' in navigator) {
  self.addEventListener('load', function() {
    navigator.serviceWorker.register('/sw.js').then(() => {
      console.log('Service Worker registered successfully.');
    }).catch(error => {
      console.log('Service Worker registration failed:', error);
    });
  })
}
const elmDiv = document.querySelector("#elm-container");
if(elmDiv) {
  Elm.Main.embed(elmDiv);
}
