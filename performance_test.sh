#!/usr/bin/env bash
set +e

RED='\033[0;31m'
GREEN='\033[1;32m'
BLUE='\033[1;34m'
WHITE='\033[1;37m'

echo -e "${BLUE}running lighthouse audit${WHITE}"
OUTPUT=$(./node_modules/lighthouse-ci/lib/cli.js https://powerful-reef-97104.herokuapp.com/ --performance=70 --seo=50 --pwa=60 --accessibility=80 --best-practices=90)

if [[ $OUTPUT = *failed* ]]; then
  echo $OUTPUT
  echo -e "${RED}ERR: performance job failed"
  exit 1
else
  echo $OUTPUT
  echo -e "${GREEN} performance job passed"
  exit 0
fi