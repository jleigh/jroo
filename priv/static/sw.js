self.addEventListener('install', (e) => {
  e.waitUntil(
    caches.open('home_page_cache').then(cache => {
      return cache.addAll(['/'])
    })
  )
})

self.addEventListener('fetch', (e) => {
  e.respondWith(
    caches.match(e.request).then((resp) => {
      if (resp) return resp;
      return fetch(e.request);
    })
  );
});