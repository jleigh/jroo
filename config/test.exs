use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :jroo, JrooWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :jroo, search_service: JrooWeb.MockSearch
config :jroo, graph_service: JrooWeb.MockGraph

config :bolt_sips, Bolt,
  hostname: 'localhost',
  port: 7687,
  pool_size: 5,
  max_overflow: 1

config :jroo, Jroo.Guardian,
  issuer: "jroo",
  secret_key: "A1O1ljT6chunDWOuysq0dLOO4v0vW4LUAcsev2mvTM+bblkZBvhU+2DvUScevQnq"

config :neo4j_sips, Neo4j,
  url: System.get_env("GRAPHENEDB_TEST_HTTP_URL"),
  basic_auth: [
    username: "jroo-dev", 
    password: System.get_env("GRAPHENEDB_TEST_PASS")
  ],
  ssl: true,
  timeout: 15_000 

config :jroo, Jroo.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "jroo_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox
