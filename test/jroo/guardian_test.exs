defmodule Jroo.GuardianTest do
  use ExUnit.Case
  alias Jroo.Guardian
  alias Jroo.Accounts
  alias Jroo.User
  alias Jroo.Repo

  describe "#subject_for_token" do
    test "returns stringyfied id" do
      user = %User{id: 123}
      assert Guardian.subject_for_token(user, %{}) == {:ok, "123"}
    end
  end

  describe "#resource_from_claims" do
    setup do
      Ecto.Adapters.SQL.Sandbox.checkout(Repo)
      Ecto.Adapters.SQL.Sandbox.mode(Repo, :auto)
      Repo.delete_all(User)
      params = %{email: "email@gmail.com", password: "yellowbird", password_confirmation: "yellowbird"}
      changeset = User.changeset(%User{}, params)
      Accounts.create_user(changeset)
      :ok
    end

    test "returns resource" do
      user = Repo.get_by(User, email: "email@gmail.com")
      result = Tuple.to_list(Guardian.resource_from_claims(%{"sub" => user.id}))
      assert List.last(result).id == user.id
    end
  end
end 