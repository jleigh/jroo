defmodule JrooWeb.UserViewTest do
  use JrooWeb.ConnCase, async: true
  alias JrooWeb.UserView

  test "renders jwt.json" do
    assert JrooWeb.UserView.render("jwt.json", %{jwt: "bob"}) == %{jwt: "bob"}
  end

  test "renders error.json" do
    assert JrooWeb.UserView.render("error.json", %{}) == %{error: "Invalid params"}
  end
end