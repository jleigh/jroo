defmodule JrooWeb.ProductControllerTest do
  use JrooWeb.ConnCase

  test "GET /product/:id", %{conn: conn} do
    conn = get conn, "/product/az-amazon-1234"
    assert html_response(conn, 200) =~ "Classic Plus 4.5 Quart Stand Kitchenaid Mixer"
  end
end