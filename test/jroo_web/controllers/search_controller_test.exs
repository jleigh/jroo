defmodule JrooWeb.SearchControllerTest do
  use JrooWeb.ConnCase

  describe "#index" do
    test "GET /search with response", %{conn: conn} do
      params = %{query: "hello"}
      conn = get conn, search_path(conn, :index, [search: params])
      assert html_response(conn, 200) =~ "Classic Plus 4.5 Quart Stand Kitchenaid Mixer"
      assert html_response(conn, 200) =~ "upc: 050946535647"
    end
    
    test "GET /search with empty response", %{conn: conn} do
      params = %{query: "empty"}
      conn = get conn, search_path(conn, :index, [search: params])
      assert html_response(conn, 200) =~ "No results found"
    end
  end
end