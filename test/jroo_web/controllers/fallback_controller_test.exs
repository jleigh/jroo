defmodule JrooWeb.FallabckControllerTest do
  use JrooWeb.ConnCase

  describe "#call" do
    test "GET", %{conn: conn} do
      conn = get conn, "/api/v1/unauthorized"
      response = json_response(conn, 401)
      assert Map.has_key?(response, "error")
    end
  end
end