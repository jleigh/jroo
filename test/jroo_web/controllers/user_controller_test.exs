defmodule JrooWeb.UserControllerTest do
  use JrooWeb.ConnCase
  alias Jroo.User
  alias Jroo.Accounts

  describe "#create" do
    test "POST /api/v1/sign_up with response", %{conn: conn} do
      params = %{email: "hello@gmail.com", password: "helloworld", password_confirmation: "helloworld"}
      conn = post conn, user_path(conn, :create, [user: params])
      response = json_response(conn, 200)
      assert Map.has_key?(response, "jwt")
    end
    
    test "POST /search with empty response", %{conn: conn} do
      invalid_params = %{email: "hello@gmail.com"}
      conn = post conn, user_path(conn, :create, [user: invalid_params])
      response = json_response(conn, 400) 
      assert Map.has_key?(response, "error")
    end
  end

   describe "#sign_in" do
    test "POST /api/v1/sign_in with response", %{conn: conn} do
      params = %{email: "hello@gmail.com", password: "helloworld"}
      changeset = User.changeset(%User{}, params)
      with {:ok, %User{} = user} <- Accounts.create_user(changeset), {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
        conn = post conn, user_path(conn, :sign_in, params)
        response = json_response(conn, 200)
        assert Map.has_key?(response, "jwt")
      end
    end
    
    test "POST /search with empty response", %{conn: conn} do
      invalid_params = %{email: "hello_there@gmail.com", password: "helloworld"}
      conn = post conn, user_path(conn, :sign_in, invalid_params)
      response = json_response(conn, 400)
      assert Map.has_key?(response, "error")
    end
  end
end