defmodule JrooWeb.GraphNodeTest do
  use ExUnit.Case
  alias JrooWeb.GraphNode, as: GraphNode

  describe "#find" do
    test "makes the call with the correct query" do
      GraphNode.find("az-amazon-1234")
      |> Map.get("query")
      |> (&(assert(&1 == "MATCH (p:Product {uuid: 'az-amazon-1234'}) RETURN p"))).()
    end
  end

  describe "#batch find" do
    test "makes the call with the correct query" do
      GraphNode.batch_find(["1234", "abcd"])
      |> List.first
      |> Map.get("query")
      |> (&(assert(&1 == "UNWIND [{uuid: '1234'}, {uuid: 'abcd'}] as row MATCH (p:Product {uuid: row.uuid}) return p"))).()
    end
  end
end
