defmodule JrooWeb.SearchServiceTest do
  use ExUnit.Case
  alias JrooWeb.SearchService, as: Search

  describe "#search" do
    test "search recieves the query" do
      Search.search("KitchenAid")
      |> List.first
      |> (&(assert(&1 == "az-amazon-1234"))).()
    end
  end
end