describe('Home page', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('displays the main site content', () => {
    cy.contains('Yroo').should('be.visible');
    cy.contains('Search').should('be.visible');
  });

  it('performs a search when you type and enter in the search bar', () => {
    cy.get('.search-bar').type('shark {enter}');
    cy.contains('Search').should('be.visible');
    cy.contains('upc: 790683736371').should('be.visible');
  });
});
