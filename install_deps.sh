#!/usr/bin/env bash
set -ev

echo "installing cloud dependencies"
echo "deb http://packages.cloud.google.com/apt cloud-sdk-jessie main" | tee /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
curl -sL https://deb.nodesource.com/setup_6.x | bash -
wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && dpkg -i erlang-solutions_1.0_all.deb

echo "installing cloud sdk, elixir & npm"
apt-get update && apt-get install -y google-cloud-sdk
apt-get install -y esl-erlang && apt-get install -y elixir
apt-get install -y nodejs && apt-get install -y npm

echo "installing phoenix and dependencies"
mix local.hex --force
mix local.rebar --force
mix archive.install https://github.com/phoenixframework/archives/raw/master/phx_new.ez
mix deps.get && MIX_ENV=prod mix deps.compile

echo "Preparing build"
mix release.init
# cd assets && npm install && ./node_modules/brunch/bin/brunch build -p
# cd .. && MIX_ENV=prod mix phx.digest
# MIX_ENV=prod mix release --env=prod