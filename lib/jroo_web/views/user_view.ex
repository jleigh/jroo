defmodule JrooWeb.UserView do
  use JrooWeb, :view

  def render("jwt.json", %{jwt: jwt}) do
    %{jwt: jwt}
  end

  def render("error.json", %{}) do
    %{error: "Invalid params"}
  end
end