defmodule JrooWeb.SearchView do
  use JrooWeb, :view

  def no_results_message() do
    "No results found"
  end
end