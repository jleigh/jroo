defmodule JrooWeb.HomeView do
  use JrooWeb, :view

  def title() do
    "Yroo"
  end
end