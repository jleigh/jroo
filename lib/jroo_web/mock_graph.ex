defmodule JrooWeb.MockGraph do
  def query(query) do
    if Enum.member?(empty_queries(), query) do
      determine_empty_strategy(query)
    else
      determine_strategy(query)
    end
  end

  def mock_batch_find(query) do
    {:ok, results(query)}
  end

  def mock_find(query) do
    {:ok, results(query)}
  end

  defp results(query) do
    [ %{
        "p" => %{
           "advertiser_id" => 1,
           "asin" => "B00063ULMI",
           "currency" => "USD",
           "current_price" => 187.5,
           "image_url" => "https://i5.walmartimages.com/asr/be0574a3-4010-4494-af7b-462a8f5b336a_1.e730ffa35b28dec5a9f393167399558a.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF",
           "name" => "Classic Plus 4.5 Quart Stand Kitchenaid Mixer",
           "review_count" => 1140,
           "star_count" => 4,
           "upc" => "050946535647",
           "url" => "https://www.amazon.com/KitchenAid-KSM75WH-Classic-4-5-Quart-Tilt-Head/dp/B00063ULMI/ref=sr_1_1?s=home-garden&ie=UTF8&qid=1532131402&sr=1-1&keywords=KitchenAid+KSM75WH+Classic+Plus+Series+4.5-Quart+Tilt-Head+Stand+Mixer%2C+White&refinements=p_n_srvg_2947266011%3A2972996011",
           "uuid" => "az-amazon-1234",
           "query" => query
        }
      }
    ]
  end

  defp determine_strategy(query) do
    if String.starts_with?(query, "UNWIND") do
      mock_batch_find(query)
    else
      mock_find(query)
    end
  end

  defp determine_empty_strategy(query) do
    if String.starts_with?(query, "UNWIND") do
      {:ok, []}
    else
      []
    end
  end

  defp empty_queries() do
    [
      "MATCH (p:Product {uuid: ''}) RETURN p",
      "UNWIND [] as row MATCH (p:Product {uuid: row.uuid}) return p"
    ]
  end
end