
defmodule JrooWeb.Router do
  use JrooWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end
  
  scope "/api/v1", JrooWeb do
    pipe_through :api

    post "/sign_up", UserController, :create
    post "/sign_in", UserController, :sign_in
    get  "/unauthorized", FallbackController, :call 
  end

  scope "/", JrooWeb do
    pipe_through :browser # Use the default browser stack
    get "/", HomeController, :index
  end

  scope "/search", JrooWeb do
    pipe_through :browser # Use the default browser stack
    get "/", SearchController, :index
  end

  scope "/product", JrooWeb do
    pipe_through :browser # Use the default browser stack
    get "/:id", ProductController, :show
  end
end
