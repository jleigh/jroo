defmodule JrooWeb.GraphNode do
  
  def find(id) do
    "MATCH (p:Product {uuid: '#{id}'}) RETURN p"
    |> graph_module().query
    |> Tuple.to_list
    |> List.last
    |> List.first
    |> Map.get("p")
  end

  def batch_find(ids) do
    query = "UNWIND #{build_batch(ids)} as row MATCH (p:Product {uuid: row.uuid}) return p"
    resp = graph_module().query(query)
    list = Tuple.to_list(resp)
    items = List.last(list)
    Enum.map(items, fn item -> Map.get(item, "p") end)
  end

  defp build_batch(ids) do
    id_string = Enum.map(ids, fn id -> "{uuid: '#{id}'}" end)
    batch = Enum.join(id_string, ", ")
    "[#{batch}]"
  end

  defp graph_module() do
    Application.get_env(:jroo, :graph_service)
  end
end