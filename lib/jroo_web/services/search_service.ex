defmodule JrooWeb.SearchService do
  
  def search(term) do
    search_module().search(term)
    |> extract
    |> Enum.map(fn hit -> hit[:_source][:prod_id] end)
  end

  defp extract(resp) do
    resp |> Tuple.to_list 
    |> List.last 
    |> Map.get(:body)
    |> Map.get(:hits) 
    |> Map.get(:hits)
  end

  defp search_module() do
    Application.get_env(:jroo, :search_service)
  end
end