defmodule JrooWeb.SearchController do
  use JrooWeb, :controller
  import JrooWeb.SearchService

  def index(conn, params) do
    ids = search(params["search"]["query"])
    products = Product.batch_find(ids)
    render conn, "index.html", products: products
  end
end