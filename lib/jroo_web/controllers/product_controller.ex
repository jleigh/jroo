defmodule JrooWeb.ProductController do
  use JrooWeb, :controller

  def show(conn, params) do
    p = Product.find(params["id"])
    render conn, "show.html", product: p
  end
end