defmodule JrooWeb.FallbackController do
  use JrooWeb, :controller
  
  def call(conn, _params) do
    conn
    |> put_status(:unauthorized)
    |> json(%{error: "Loging error"})
  end
end