defmodule JrooWeb.HomeController do
  use JrooWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end