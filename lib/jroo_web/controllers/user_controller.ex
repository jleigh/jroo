defmodule JrooWeb.UserController do
  use JrooWeb, :controller
  alias Jroo.Guardian
  alias Jroo.User
  alias Jroo.Accounts

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)
    with {:ok, %User{} = user} <- Accounts.create_user(changeset),
         {:ok, token, _claims} <- Guardian.encode_and_sign(user) do
      conn |> render("jwt.json", jwt: token)
    else {:error, changeset} ->
      conn 
        |> put_status(400)
        |> render("error.json", %{})
    end
  end

  def sign_in(conn, %{"email" => email, "password" => password}) do
    case Accounts.token_sign_in(email, password) do
      {:ok, token, _claims} ->
        conn |> render("jwt.json", jwt: token)
      {:error, :unauthorized} ->
        conn 
          |> put_status(400)
          |> render("error.json", %{})
    end
  end
end