defmodule JrooWeb.Search do
  @url Elastix.config(:uri)
  @index "products"
  
  def search(term) do
    query = build_query(term)
    Elastix.Search.search(@url, @index, ["product"], query)
  end

  def build_query(term) do
    %{
      query: %{
        match: %{
          name: term
        }
      }
    }
  end
end