defmodule JrooWeb.Graph do
  alias Neo4j.Sips, as: Neo4j
  
  def query(query) do
    Neo4j.query(Neo4j.conn, query)
  end
end