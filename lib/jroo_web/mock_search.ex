defmodule JrooWeb.MockSearch do
  def search(term) do
    if term == "empty" do
      empty_response()
    else
      mock_response(term)
    end 
  end

  def empty_response() do
    {:ok,
      %HTTPoison.Response{
        body: %{
          hits: %{
            hits: [],
            total: 0
          },
        },
        request_url: "http://test_search.com",
        status_code: 200
      }
    }
  end

  def mock_response(term) do
    {:ok,
      %HTTPoison.Response{
        body: %{
          hits: %{
            hits: [
              %{
                  _id: "test_id",
                  _index: "products",
                  _score: 0.28,
                  _source: %{
                  advertiser: "Amazon",
                  brand: "KitchenAid",
                  mpn: "KSM155GBAZ",
                  name: term,
                  upc: "883049352633",
                  prod_id: "az-amazon-1234"
                },
                _type: "product"
              }
            ],
            total: 1
          },
        },
        request_url: "http://test_search.com",
        status_code: 200
      }
    }
  end
end